
const createExpoWebpackConfigAsync = require('@expo/webpack-config');
const path = require('path');

const aliases = {
    'src': path.resolve(__dirname, 'src'),
    'assets': path.resolve(__dirname, 'assets'),
};

const tsLoaderRules = {
    test: /\.(png|jpg|jpeg|mp3|mp4)$/,
    loader: 'file-loader',
};

const modelLoaderConfiguration = {
    test: /\.(png|jp?g|mp3|mp4)$/,
    use: {
        loader: 'file-loader',
    },
};

const imageLoader = {
    test: /\.(jpg|png)$/,
    use: {
        loader: 'url-loader',
    },
};

module.exports = async function(env, argv) {

    const config = await createExpoWebpackConfigAsync(env, argv);

    config.resolve.alias = {
        ...config.resolve.alias,
        ...aliases,
    };

    config.module.rules = [
        ...config.module.rules,
        tsLoaderRules,
    ];

    // Other configuration options

    return config;
}
