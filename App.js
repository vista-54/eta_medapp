import React, { useState, Component } from "react";

import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'

import reducer from './src/reducers'

const middleware = applyMiddleware(thunkMiddleware)
const store = createStore(reducer, middleware)
import AppNavigator from './src/navigation/AppNavigator'
import { func } from './src/constants';
import { AppLoading } from 'expo';
// "react-navigation": "4.0.10",


export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentSongData: {
                album: 'Swimming',
                artist: 'Mac Miller',
                image: 'swimming',
                length: 312,
                title: 'So It Goes'
            },
            isLoading: true,
            toggleTabBar: false
        };

        this.changeSong = this.changeSong.bind(this);
        this.setToggleTabBar = this.setToggleTabBar.bind(this);
    }

    setToggleTabBar() {
        this.setState(({ toggleTabBar }) => ({
            toggleTabBar: !toggleTabBar
        }));
    }

    changeSong(data) {
        this.setState({
            currentSongData: data
        });
    }

    render() {
        const { currentSongData, isLoading, toggleTabBar } = this.state;
        if (isLoading) {
            return (
                <AppLoading
                    onFinish={() => this.setState({ isLoading: false })}
                    startAsync={func.loadAssetsAsync}
                />
            );
        }
        return (
            <React.Fragment>
            <Provider store={store}>
                <AppNavigator
                    screenProps={{
                        currentSongData,
                        changeSong: this.changeSong,
                        setToggleTabBar: this.setToggleTabBar,
                        toggleTabBarState: toggleTabBar
                    }}
                />
            </Provider>
            </React.Fragment>
        )
    }
}
