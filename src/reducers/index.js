import { combineReducers } from 'redux'
import {
    GET_ARTICLES,
    GET_CATEGORIES,
    GET_SUBCATEGORIES,
    GET_SELECTEDITEM,
    SET_VIDEO,
    SET_PLAYERBAR,
    SET_PLAYERBAR_PLAY,
    SET_PLAY_COUNTER
} from "../actions/actionTypes";

const initialState = {
    articles: [],
    categories: [],
    subcategories: [],
    newsSelectedItem: false,
    setVideo: [],
    setPlayerBar: [],
    setPlayerBarPlay: [],
    setPlayCounter: []
};

const selectedItem = (state = [], action) => {
    switch (action.type) {
        case GET_SELECTEDITEM:
            return [...state, Object.assign({}, action.payload)];
        default:
            return state
    }
}


const articles = (state = initialState, action) => {
    switch (action.type) {
        case GET_ARTICLES:
            return { ...state, articles: action.payload }
        default:
            return state
    }
}


const categories = (state = initialState, action) => {
    switch (action.type) {
        case GET_CATEGORIES:
            return { ...state, categories: action.payload }
        default:
            return state
    }
}

const subcategories = (state = initialState, action) => {
    switch (action.type) {
        case GET_SUBCATEGORIES:
            return { ...state, subcategories: action.payload }
        default:
            return state
    }
}

const video = (state = initialState, action) => {
    switch (action.type) {
        case SET_VIDEO:
            return { ...state, setVideo: action.payload }
        default:
            return state
    }
}

const player = (state = initialState, action) => {
    switch (action.type) {
        case SET_PLAYERBAR:
            return { ...state, setPlayerBar: action.payload }
        default:
            return state
    }
}

const playerBarPlay = (state = initialState, action) => {
    switch (action.type) {
        case SET_PLAYERBAR_PLAY:
            return { ...state, setPlayerBarPlay: action.payload }
        default:
            return state
    }
}
const playCounter = (state = initialState, action) => {
    switch (action.type) {
        case SET_PLAY_COUNTER:
            return { ...state, setPlayCounter: action.payload }
        default:
            return state
    }
}

const rootReducer = combineReducers({
    articles, categories, subcategories, selectedItem, video, player, playerBarPlay, playCounter
});

export default rootReducer
