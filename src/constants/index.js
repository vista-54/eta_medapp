import Colors from './Colors';
import device from './device';
import fonts from './fonts';
import func from './functions';
import gStyle from './globalStyles';
import images from './preloadImages';

export { Colors, device, fonts, func, gStyle, images };
