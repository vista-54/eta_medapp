import React from 'react';
import PropTypes from 'prop-types';
// import { BottomTabBar } from 'react-navigation';
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';

// components
import PlayerBar from './PlayerBar';
import BottomTab from "./BottomTab";

const CustomTabBar = props => {
  const {
    screenProps: { currentSongData, toggleTabBarState }
  } = props;

  return toggleTabBarState ? null : (
    <React.Fragment>
      <PlayerBar song={currentSongData} />
      <BottomTabBar {...props} />
      {/*<BottomTab {...props} />*/}
    </React.Fragment>
  );
};

// CustomTabBar.propTypes = {
//   // required
//   navigation: PropTypes.object.isRequired,
//   screenProps: PropTypes.object.isRequired
// };

export default CustomTabBar;
