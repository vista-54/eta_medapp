export function onPlayPausePressed(stateOfPause, tmp) {
    if (stateOfPause) {
        tmp.pauseAsync();
        stateOfPause = false;
    } else {
        tmp.playAsync();
        stateOfPause = true;
    }
    return stateOfPause;
};
