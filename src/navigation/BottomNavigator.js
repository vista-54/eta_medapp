import React from 'react'
import {
    createStackNavigator,
} from 'react-navigation-stack'
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';
import Sleep from "../screens/Sleep";
import Music from "../screens/Music";
import Sounds from "../screens/Sounds"
import Meditation from "../screens/Meditation";
import Player from "../screens/Player";
import SplashScreen from "../screens/SplashScreen";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import PlayerBar from './PlayerBar';
import CustomTabBar from "./CustomTabBar";

// const TabBarComponent = props => {
//     const {
//         screenProps: { currentSongData, toggleTabBarState }
//     } = props;
// return toggleTabBarState ? null : (
//     <React.Fragment>
// <PlayerBar song={currentSongData} />
// <BottomTabBar {...props} />
// </React.Fragment>
// );
// };

//Splashscreen view
const HomeStack = createStackNavigator(
    {
        Home: SplashScreen,
    },
    {
        headerMode: 'none',
    },
);

HomeStack.navigationOptions = {
    tabBarLabel: 'Home',
    tabBarOptions: {
        activeTintColor: '#000',
        inactiveTintColor: '#879b99',
    },
    tabBarIcon: ({ focused }) => (
        <View style={styles.group2}>
            <Image
                source={require("../assets/images/meditate.png")}
                resizeMode="contain"
                style={styles.meditate}
                imageStyle={styles.meditate_imageStyle}
            />
            {/*<Text style={styles.text2}>Meditation</Text>*/}
        </View>
    ),
};

//Meditation category
const MeditationStack = createStackNavigator(
    {
        Meditation: Meditation,
    },
    {
        headerMode: 'none',
    },
);

MeditationStack.navigationOptions = {
    tabBarLabel: 'Meditation',
    tabBarOptions: {
        activeTintColor: '#000',
        inactiveTintColor: '#879b99',
    },
    tabBarIcon: ({ focused }) => (
        <View style={styles.group2}>
            <Image
                source={focused ? require("../assets/images/Meditate_full.png") : require("../assets/images/meditate.png")}
                resizeMode="contain"
                style={styles.meditate}
                imageStyle={styles.meditate_imageStyle}
            />
        </View>    ),
};

//SLeep Category
const SleepStack = createStackNavigator(
    {
        Sleep: Sleep,
    },
    {
        headerMode: 'none',
    },
);

SleepStack.navigationOptions = {
    tabBarLabel: 'Sleep',
    tabBarOptions: {
        activeTintColor: '#000',
        inactiveTintColor: '#879b99',
    },
    tabBarIcon: ({ focused }) => (
        <View style={styles.group}>
            <Image
                source={focused ? require("../assets/images/Sleep_full.png") : require("../assets/images/sleep1.png")}
                resizeMode="contain"
                style={styles.sleep}
                imageStyle={styles.sleep_imageStyle}
            />
            {/*<Text style={styles.text}>Sleep</Text>*/}
        </View>
    ),
};

//Music Category
const MusicStack = createStackNavigator(
    {
        Music: Music,
    },
    {
        headerMode: 'none',
    },
);

MusicStack.navigationOptions = {
    tabBarLabel: 'Music',
    tabBarOptions: {
        activeTintColor: '#000',
        inactiveTintColor: '#879b99',
    },
    tabBarIcon: ({ focused }) => (
        <View style={styles.group3}>
            <Image
                source={focused ? require("../assets/images/Music_full.png") : require("../assets/images/Music1.png")}
                resizeMode="contain"
                style={styles.music}
                imageStyle={styles.music_imageStyle}
            />
            {/*<Text style={styles.text3}>Music</Text>*/}
        </View>
    ),
};

//Sound Category
const SoundsStack = createStackNavigator(
    {
        Sounds: Sounds,
    },
    {
        headerMode: 'none',
    },
);

SoundsStack.navigationOptions = {
    tabBarLabel: 'Sounds',
    tabBarOptions: {
        activeTintColor: '#000',
        inactiveTintColor: '#879b99',
    },
    tabBarIcon: ({ focused }) => (
        <View style={styles.group}>
            <Image
                source={focused ? require("../assets/images/Sound.png") : require("../assets/images/Sound_empty.png")}
                resizeMode="contain"
                style={styles.sleep}
                imageStyle={styles.sleep_imageStyle}
            />
            {/*<Text style={styles.text}>Sleep</Text>*/}
        </View>
    ),
};

// Player Category
const PlayerStack = createStackNavigator(
    {
        Player: Player,
    },
    {
        headerMode: 'none',
    },
);

PlayerStack.navigationOptions = {
    tabBarLabel: 'Player',
    header: {
        visible: false
    },
    // { navigation }) => {
    // if( navigation.state.index===1 ){
    //     return {
    //         tabBarVisible: false,
    //     };
    // }
    // return {
    //     tabBarVisible: false,
    // }
    tabBarOptions: {
        activeTintColor: '#879b99',
        inactiveTintColor: '#879b99',
        // labelStyle: {
        //     fontWeight: "bold"
        // }
    },
    tabBarIcon: ({ focused }) => (
        <View style={styles.group3}>
            <Image
                // source={require("../assets/images/Player.png")}
                source={focused ? require("../assets/images/Player_full.png") : require("../assets/images/Player.png")}
                resizeMode="contain"
                style={styles.music}
                imageStyle={styles.music_imageStyle}
            />
            {/*<Text style={styles.text3}>Music</Text>*/}
        </View>
    ),
};


//Creates bottom navigator
const BottomTabNavigator = createBottomTabNavigator({
    MeditationStack,
    SleepStack,
    MusicStack,
    SoundsStack,
    PlayerStack,
},
    {
        tabBarComponent: props => (
            <CustomTabBar {...props} style={{
                // backgroundColor: 'transparent',
                borderTopColor: '#605F60',
                backgroundColor: "#EFFFEC",
                // borderTopLeftRadius: 20,
                // borderTopRightRadius: 20,
                // borderTopWidth: 0,
                // position: 'absolute',
                // display: 'none'
                // elevation: 0,
                // paddingTop: 15,

            }} />
        ),
        tabBarOptions: {
            style: {
                // borderTopWidth: 0,
                // backgroundColor: 'rgba(0,128,0,0.15)',
                // borderTopRightRadius: 20,
                // borderTopLeftRadius: 20,
                // height: 55,
                // paddingBottom: 5,
            }
        },
    }

    );


const styles = StyleSheet.create({
    container: {},
    rect2: {
        width: 375,
        height: 84,
        backgroundColor: "rgba(255,255,255,1)"
    },
    group5: {
        width: 297,
        height: 60,
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 2,
        marginLeft: 39
    },
    group: {
        width: 40,
        height: 40,
        alignItems: "center"
    },
    sleep: {
        width: 40,
        height: 40
    },
    text: {
        color: "rgba(135,155,153,1)",
        fontSize: 12,
        // fontFamily: "open-sans-600"
    },
    group2: {
        width: 64,
        height: 40,
        alignItems: "center"

    },
    meditate: {
        width: 40,
        height: 40

    },
    text2: {
        color: "rgba(135,155,153,1)",
        fontSize: 12,
        // fontFamily: "open-sans-600",
        textAlign: "center"
    },
    group3: {
        width: 40,
        height: 40,
        alignItems: "center"
    },
    music: {
        width: 40,
        height: 40,
    },
    text3: {
        color: "rgba(135,155,153,1)",
        fontSize: 12,
        // fontFamily: "open-sans-600",
        textAlign: "center"
    },
    group4: {
        width: 40,
        height: 55,
        alignItems: "center"
    },
    more: {
        width: 40,
        height: 40
    },
    text4: {
        color: "rgba(135,155,153,1)",
        fontSize: 12,
        // fontFamily: "open-sans-600",
        textAlign: "center"
    }
});
export default BottomTabNavigator;
