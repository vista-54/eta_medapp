import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { device } from '../constants';
import { gStyle } from '../constants';
import { Colors } from '../constants';
import {connect} from "react-redux";
import { withNavigation, NavigationActions, SwitchActions } from 'react-navigation';
import Player from '../screens/Player';
import {onPlayPausePressed} from './helper'
import {setPlayerBarPlay, setPlayCounter} from "../actions/collection";


class PlayerBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            favorited: false,
            paused: false,
            setPlayerBar: [],
            setPlayerBarPlay: [],
            setPlayCounter: []
        };
        this.toggleFavorite = this.toggleFavorite.bind(this);
        this.togglePlay = this.togglePlay.bind(this);
        this.counter = this.counter.bind(this);

    }

    toggleFavorite() {
        this.setState(prev => ({
            favorited: !prev.favorited
        }));
    }

    togglePlay() {
            let playStopFunc = this.props.setPlayerBarPlay.setPlayerBarPlay;
            let tmp = this.props.setVideo.setVideo;
            const nowPlaying = tmp[tmp.length-1];
            const playState = onPlayPausePressed(playStopFunc,nowPlaying );
            console.log(playStopFunc);
            this.props.addPlayerBarPlay(playState);
            this.setState(prev => ({
                paused: !prev.paused
            }));
    }

    counter() {
    this.setState({
        counter: 1
    })
    }

    render() {
        const {navigation, song} = this.props;
        // const {navigate} = this.props.navigation;

        const {favorited, paused} = this.state;
        const favoriteColor = favorited ? Colors.black : Colors.white;
        const favoriteIcon = favorited ? 'heart' : 'heart-o';
        const iconPlay = paused ? 'play-circle' : 'pause-circle';
        // const pla = true;
        const pla = this.props.setPlayerBarPlay.setPlayerBarPlay;
        // if (pla === true) {{this.counter()}}
        const counter = this.props.setPlayCounter.setPlayCounter;
        console.log('counter', counter);
        // this.props.addPlayCounter(counter);
        // TODO from redux state, and pause icon if only started
        if (pla === true || counter === 1) {
            return (
                <TouchableOpacity activeOpacity={1}
                                  onPress={() =>
                                      this.props.navigation.dispatch(SwitchActions.jumpTo({ routeName: 'PlayerStack'}))}
                                  // this.props.navigation.dispatch(NavigationActions.navigate('Player'))}
                                  // navigation.push("PlayerStack")}
                                  style={styles.container}
                >
                    <TouchableOpacity
                        activeOpacity={gStyle.activeOpacity}
                        hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}
                        onPress={this.toggleFavorite}
                        style={styles.containerIcon}
                    >
                        <FontAwesome color={Colors.black} name={favoriteIcon} size={20}/>
                    </TouchableOpacity>
                    {song && (
                        <View>
                            <View style={styles.containerSong}>
                                <Text
                                    style={styles.title}>{`${this.props.setPlayerBar.setPlayerBar
                                    [this.props.setPlayerBar.setPlayerBar.length - 1]}`}
                                </Text>
                            </View>
                            {/*<View style={[gStyle.flexRowCenter, gStyle.mTHalf]}>*/}
                            {/*    <FontAwesome*/}
                            {/*        color={Colors.brandPrimary}*/}
                            {/*        name="bluetooth"*/}
                            {/*        size={14}*/}
                            {/*    />*/}
                            {/*    <Text style={styles.device}>'a'</Text>*/}
                            {/*</View>*/}
                        </View>
                    )}
                    <TouchableOpacity
                        activeOpacity={gStyle.activeOpacity}
                        hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}
                        onPress={this.togglePlay}
                        style={styles.containerIcon}
                    >
                        <FontAwesome color={Colors.black} name={iconPlay} size={28}/>
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
        else {
            return (
                <View></View>
            );
        }
    }
}
const styles = StyleSheet.create({
    container: {
        alignSelf: 'center',
        // backgroundColor: Colors.white,
        backgroundColor: Colors.greenLight,
        borderTopColor: Colors.black,
        borderBottomColor: Colors.black,
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 8,
        width: '100%',
        // position: 'relative'
    },
    containerIcon: {
        ...gStyle.flexCenter,
        width: 50
    },
    containerSong: {
        ...gStyle.flexRowCenter,
        overflow: 'hidden',
        width: device.width - 100
    },
    title: {
        ...gStyle.textSpotify12,
        color: Colors.black
    },
    artist: {
        ...gStyle.textSpotify12,
        color: Colors.greyLight
    },
    device: {
        ...gStyle.textSpotify10,
        color: Colors.brandPrimary,
        marginLeft: 4,
        textTransform: 'uppercase'
    }
});
const mapDispatchToProps = dispatch => {
    return{
        addPlayerBarPlay: (playerBarPlay) => {
            dispatch(setPlayerBarPlay(playerBarPlay))
        },
        addPlayCounter: (playCounter) => {
            dispatch(setPlayCounter(playCounter))
        }
    }
}

const mapStateToProps = state => {
    return {
        setPlayerBar: state.player,
        setPlayerBarPlay: state.playerBarPlay,
        setVideo: state.video,
        setPlayCounter: state.playCounter
    }
}

export default withNavigation(connect (mapStateToProps, mapDispatchToProps) (PlayerBar))


