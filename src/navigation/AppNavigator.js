import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import ModalRoutes from './ModalRoutes';

import BottomNavigator from "./BottomNavigator";
import CustomTabBar from "./CustomTabBar";
import SplashScreen from "../screens/SplashScreen";
import Player from "../screens/Player"
export default createAppContainer(
    ///// old version used SwitchNavigator. uncomment here and in BottomNavigator uncomment Player
    // createStackNavigator
createStackNavigator
(
    {
    SplashScreen,
    Main: BottomNavigator,
    // Player: {
    //     screen: Player,
    //     navigationOptions: {
    //         gesturesEnabled: false
    //     }
    // },
},
{
    headerMode: 'none',
    initialRouteName: 'SplashScreen',
    mode: 'modal',
    transitionConfig: ModalRoutes,
    transparentCard: true,
    // gesturesEnabled: true

},
));
