import Firebase, { db } from '../config/Firebase.js'
import {GET_ARTICLES, GET_CATEGORIES, GET_SUBCATEGORIES,
    GET_SELECTEDITEM, SET_VIDEO, SET_PLAYERBAR, SET_PLAYERBAR_PLAY, SET_PLAY_COUNTER} from "./actionTypes";

export const getSelectedItem = selectedItem => (
    {
        type: GET_SELECTEDITEM,
        payload: selectedItem,
    }
);

export const setVideo = video => (
    {
        type: SET_VIDEO,
        payload: video,
    }
);

export const setPlayerBar = player => (
    {
        type: SET_PLAYERBAR,
        payload: player,
    }
);

export const setPlayerBarPlay = playerBarPlay => (
    {
        type: SET_PLAYERBAR_PLAY,
        payload: playerBarPlay,
    }
);
export const setPlayCounter = playCounter => (
    {
        type: SET_PLAY_COUNTER,
        payload: playCounter,
    }
);

export const getArticles = () => {
    return async (dispatch, articles) => {
        try {
            const articles = await db
                .collection("articles")
                // .doc()
                .get()
                .then(function(querySnapshot) {
                    const articles = []

                    querySnapshot.forEach(function(doc) {
                    // doc.data() is never undefined for query doc snapshots
                    // const articles = []
                    //     articles.push(doc.data())
                        articles.push({
                            ...doc.data(),
                            key: doc.id,
                        })
                    // console.log(doc.id, " ARTical=========> ", doc.data());
                });
                    // console.log( " ART++-----+++++> ", articles);
                    dispatch({
                        type: GET_ARTICLES,
                        payload: articles
                    })
            }).catch(function(error) {
                console.log("Error getting document:::::::::", error);
            });
            // console.log( " ++-----+++++> ", articles);
            // dispatch({
            //     type: GET_ARTICLES,
            //     payload: articles
            // })
        } catch (e) {
            alert(e)
        }
    }
};

export const getCategories = () => {
    return async (dispatch, categories) => {
        try {
            const categories = await db
                .collection("categories")
                // .doc()
                .get()
                .then(function(querySnapshot) {
                    const categories = []

                    querySnapshot.forEach(function(doc) {
                        // doc.data() is never undefined for query doc snapshots
                        // const articles = []
                        //     articles.push(doc.data())
                        categories.push({
                            ...doc.data(),
                            key: doc.id,
                        })
                        // console.log(doc.id, " CAT=========> ", doc.data());
                    });
                    // console.log( " categories++-----+++++> ", categories);
                    dispatch({
                        type: GET_CATEGORIES,
                        payload: categories
                    })
                }).catch(function(error) {
                    console.log("Error getting document:::::::::", error);
                });
            // console.log( " ++-----+++++> ", articles);
            // dispatch({
            //     type: GET_ARTICLES,
            //     payload: articles
            // })
        } catch (e) {
            alert(e)
        }
    }
};

export const getSubcategories = () => {
    return async (dispatch, subcategories) => {
        try {
            const subcategories = await db
                .collection("subcategories")
                // .doc()
                .get()
                .then(function(querySnapshot) {
                    const subcategories = []

                    querySnapshot.forEach(function(doc) {
                        // doc.data() is never undefined for query doc snapshots
                        // const articles = []
                        //     articles.push(doc.data())
                        subcategories.push({
                            ...doc.data(),
                            key: doc.id,
                        })
                        // console.log(doc.id, " SUBCAT=========> ", doc.data());
                    });
                    // console.log( " subcategories++-----+++++> ", subcategories);
                    dispatch({
                        type: GET_SUBCATEGORIES,
                        payload: subcategories
                    })
                }).catch(function(error) {
                    console.log("Error getting document:::::::::", error);
                });
            // console.log( " ++-----+++++> ", articles);
            // dispatch({
            //     type: GET_ARTICLES,
            //     payload: articles
            // })
        } catch (e) {
            alert(e)
        }
    }
};
