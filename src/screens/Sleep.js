import React from "react";
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  ImageBackground,
} from "react-native";
import { activateKeepAwake } from 'expo-keep-awake';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getArticles,
  getCategories,
  getSubcategories,
  getSelectedItem
} from '../actions/collection';
import {Tile} from 'react-native-elements';
import { MaterialIcons } from "@expo/vector-icons";
import { gStyle } from '../constants';

console.disableYellowBox = true;

const LOCKED = "lock";
const NEW = "fiber-new";

class Sleep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      subcategoryObjects: [],
      articles: [],
      selectedItem: [],
      selectedID: null,
      selectedSubCat: 12
    };
    this.counter = 1;
    this.dataCrunchingForCategories = this.dataCrunchingForCategories.bind(this);
    this.categoryOnPress = this.categoryOnPress.bind(this);
  };

  componentWillMount(){
    this.dataCrunchingForCategories();
  }
  static navigationOptions = {
    title: 'Welcome',
  };

  _activate = () => {
    activateKeepAwake();
  };

  dataCrunchingForCategories = () => {
    let subcatObjectArray = [];
    let articleIDsForSubcategory = [];
    let articlesOfParentCategory = [];
    let categoryObject = this.props.categories.find(x => x.name === "Sleep");
    let subcategoriesList = categoryObject.subcategories;
    self = this;

    subcategoriesList.map(function(id){
      let subcaObject = self.props.subcategories.find(x => x.id === id);
      subcatObjectArray.push(subcaObject);
      articleIDsForSubcategory.push.apply(articleIDsForSubcategory, subcaObject.articles);
    });

    let a = new Object();
    a.name = "All";
    a.id = 12;
    subcatObjectArray.unshift(a)

    articleIDsForSubcategory.map(function(id){
      let articleObject = self.props.articles.find(x => x.id === id)
      articlesOfParentCategory.push(articleObject);
    })
    this.setState({
      articles: articlesOfParentCategory,
      subcategoryObjects: subcatObjectArray
    })
  };

  categoryOnPress = (id) => {
    if (id === 12) {
      let subcatObjectArray = [];
      let articleIDsForSubcategory = [];
      let articlesOfParentCategory = [];
      let categoryObject = this.props.categories.find(x => x.name === "Sleep");
      let subcategoriesList = categoryObject.subcategories;
      self = this;

      subcategoriesList.map(function(id){
        let subcaObject = self.props.subcategories.find(x => x.id === id);
        subcatObjectArray.push(subcaObject);
        articleIDsForSubcategory.push.apply(articleIDsForSubcategory, subcaObject.articles);
      });

      let a = new Object();
      a.name = "All";
      a.id = 12;
      subcatObjectArray.unshift(a)

      articleIDsForSubcategory.map(function(id){
        let articleObject = self.props.articles.find(x => x.id === id)
        articlesOfParentCategory.push(articleObject);
      })
      this.setState({
        articles: articlesOfParentCategory,
        subcategoryObjects: subcatObjectArray
      })
    }
    else {
      let articlesOfParentCategory = [];
      let subcaObject = this.props.subcategories.find(x => x.id === id);
      self = this;

      subcaObject.articles.map(function(id2){
        let articleObject = self.props.articles.find(x => x.id === id2)
        articlesOfParentCategory.push(articleObject);
      })
      this.setState({
        articles: articlesOfParentCategory
      })
    };
  };

  render() {
    this.counter++;
    console.log('RENDERCOUNTERMeditation==', this.counter);
    return (
        <ImageBackground source={require('../assets/images/greenBamboo.jpg')} style={{width: '100%', height: '100%'}}>
          <SafeAreaView style={gStyle.containerCat}>
            <StatusBar hidden={true} />
            <FlatList style={gStyle.topFlatList}
                      horizontal={true}
                // pagingEnabled={true}
                      showsHorizontalScrollIndicator={false}
                      data={this.state.subcategoryObjects}
                      extraData={this.state}
                      renderItem={({item}) =>
                          <TouchableOpacity
                              onPress={() => {
                                this.categoryOnPress(item.id);
                                this.setState({selectedSubCat: item.id});
                              }}
                          >
                            {this.state.selectedSubCat === item.id ?
                                <Text style={gStyle.itemSelected}>{item.name}</Text>
                                :
                                <Text style={gStyle.item}>{item.name}</Text>
                            }
                          </TouchableOpacity>
                      }
            />
            <FlatList style={gStyle.flatList}
                      data={this.state.articles}
                      numColumns={2}
                      renderItem={({ item }) => (
                          <View style={{position: 'relative'}}>
                            {item.locked === true ?
                                <TouchableOpacity onPress={() => this.handleX(item)}
                                                  style={{
                                                    position: 'absolute',
                                                    right: 15,
                                                    top: 10,
                                                    padding: 5,
                                                    borderRadius: 50,
                                                    zIndex: 10
                                                  }}>
                                  <MaterialIcons
                                      name={LOCKED}
                                      size={20}
                                      color="black"
                                  />
                                </TouchableOpacity>
                                : null}

                            {item.new === true ?
                                <MaterialIcons
                                    name={NEW}
                                    size={20}
                                    color="black"
                                    style={{position: 'absolute', left: 15, top:10, padding: 5, borderRadius: 50, zIndex: 10  }}
                                />
                                : null}

                            <Tile
                                imageSrc={{uri: item.image }}
                                imageContainerStyle={gStyle.imageContainer}
                                activeOpacity={0.9}
                                title={item.title}
                                titleStyle={gStyle.title2}
                                containerStyle={gStyle.container2}
                                featured
                                onPress={() => {this._activate()
                                  this.props.navigation.push("Player", {itemObject: item});
                                }}
                            />
                          </View>
                      )}
                      keyExtractor={item => item.id}
            />
          </SafeAreaView>
        </ImageBackground>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ getArticles, getCategories, getSubcategories, getSelectedItem }, dispatch)
}

const mapStateToProps = state => {
  return {
    articles: state.articles.articles,
    categories: state.categories.categories,
    subcategories: state.subcategories.subcategories,
    selectedItem: state.selectedItem,
  }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Sleep)
