import React from "react";
import {
    BackHandler,
    Dimensions,
    ImageBackground,
    Slider,
    StyleSheet,
    Text,
    TouchableHighlight, TouchableOpacity,
    View
} from "react-native";
import { Asset } from "expo-asset";
import { Audio, Video } from "expo-av";
import * as Font from "expo-font";
import { Card } from 'react-native-elements'
import { MaterialIcons } from "@expo/vector-icons";
import { AntDesign, Feather as Icon } from "@expo/vector-icons";
import {withNavigation, NavigationEvents, SwitchActions} from 'react-navigation';
import {setPlayCounter, setPlayerBarPlay, setPlayerBar, setVideo} from "../actions/collection";
import {connect} from "react-redux";
import { Colors } from '../constants';
import {RectButton} from "react-native-gesture-handler";

const ICON_PLAY_BUTTON = "play-arrow";
const ICON_PAUSE_BUTTON = "pause";
const ICON_STOP_BUTTON = "stop";
const ICON_EXPAND_MORE = "expand-more";
const ICON_MUTED_BUTTON = "volume-mute";
const ICON_UNMUTED_BUTTON = "volume-up";
const ICON_TRACK_1 = "audiotrack";
const ICON_THUMB_1 = "speaker";
const ICON_THUMB_2 = "speaker";

const LOOPING_TYPE_ALL = 0;
const LOOPING_TYPE_ONE = 1;

const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get("window");

const BACKGROUND_COLOR = "#FFF8ED";
const DISABLED_OPACITY = 0.5;
const FONT_SIZE = 10;
const LOADING_STRING = "... loading ...";
const BUFFERING_STRING = "...buffering...";
const VIDEO_CONTAINER_HEIGHT = DEVICE_HEIGHT * 0.4;

// class Icon {
//     constructor(module, width, height) {
//         this.module = module;
//         this.width = width;
//         this.height = height;
//         Asset.fromModule(this.module).downloadAsync();
//     }
// }

class PlaylistItem {
    constructor(name, uri, isVideo, categoryName) {
        this.name = name;
        this.uri = uri;
        this.isVideo = isVideo;
        this.categoryName = categoryName;
    }
}

class Player extends React.Component {
    constructor(props) {
        super(props);
        this.index = 0;
        this.isSeeking = false;
        this.shouldPlayAtEndOfSeek = false;
        this.state = {
            showVideo: false,
            playbackInstanceName: LOADING_STRING,
            loopingType: LOOPING_TYPE_ALL,
            muted: false,
            playbackInstancePosition: null,
            playbackInstanceDuration: null,
            shouldPlay: false,
            isPlaying: false,
            isBuffering: false,
            isLoading: true,
            fontLoaded: false,
            shouldCorrectPitch: true,
            volume: 1.0,
            rate: 1.0,
            videoWidth: DEVICE_WIDTH,
            videoHeight: VIDEO_CONTAINER_HEIGHT,
            poster: false,
            useNativeControls: false,
            fullscreen: false,
            throughEarpiece: false,
            PLAYLIST: [],
            video: [],
            selectedItem: undefined,
            player: [],
            playerBarPlay: [],
            playCounter: [],
            hidden: false
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        const tempPlaylist = this.state.PLAYLIST;
        const selectedItem = this.props.navigation.getParam('itemObject');

        if (selectedItem === undefined) {
            const object = {
                image: 'https://firebasestorage.googleapis.com/v0/b/meditationapp-9d40d.appspot.com/o/images%2FMusic_Sleep_NightSky.jpg?alt=media&token=446f394e-a363-419d-99a8-9111fa4b7e66',
                title: 'No title selected',
                description: 'Please select a title in categories to play'
            }

            this.setState({
                selectedItem: object
            })
        }
        else {
            this.setState({
                selectedItem: selectedItem
            })
            const obj = new PlaylistItem(
                selectedItem.title,
                selectedItem.soundtrack,
                false,
                selectedItem.categoryName
            );

            tempPlaylist.push(obj);

            this.setState({
                PLAYLIST: tempPlaylist
        })
            console.log('this.state.PLAYLIST',this.state.PLAYLIST);

        }

        Audio.setAudioModeAsync({
            allowsRecordingIOS: false,
            staysActiveInBackground: false,
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
            playsInSilentModeIOS: true,
            shouldDuckAndroid: true,
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
            playThroughEarpieceAndroid: false
        });
        (async () => {
            await Font.loadAsync({
                ...MaterialIcons.font,
                "roboto-regular": require("../assets/fonts/Roboto-Bold.ttf"),
                "cutive-mono-regular": require("../assets/fonts/CutiveMono-Regular.ttf")
            });
            this.setState({ fontLoaded: true });
        })();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    handleBackPress = () => {
        return true;
    }

    async _loadNewPlaybackInstance(playing) {
        this.playbackInstance = null;
        const source = { uri: this.state.PLAYLIST[this.index].uri };
        console.log('this.state.PLAYLIST',this.state.PLAYLIST);

        const initialStatus = {
            // shouldPlay changed to "true" from "playing". now plays audio immediately.
            shouldPlay: true,
            rate: this.state.rate,
            shouldCorrectPitch: this.state.shouldCorrectPitch,
            volume: this.state.volume,
            isMuted: this.state.muted,
            isLooping: this.state.loopingType === LOOPING_TYPE_ONE
            // // UNCOMMENT THIS TO TEST THE OLD androidImplementation:
            // androidImplementation: 'MediaPlayer',
        };

        if (this.state.PLAYLIST[this.index].isVideo) {
            console.log(this._onPlaybackStatusUpdate);
            await this._video.loadAsync(source, initialStatus);
            // this._video.onPlaybackStatusUpdate(this._onPlaybackStatusUpdate);
            this.playbackInstance = this._video;
            const status = await this._video.getStatusAsync();
        } else {
            const { sound, status } = await Audio.Sound.createAsync(
                source,
                initialStatus,
                this._onPlaybackStatusUpdate
            );
//////Redux
            let tmp = this.props.setVideo.setVideo;
            let tmpplayerBarPlay = this.props.setPlayerBarPlay.setPlayerBarPlay;
            let tmpplayer = [];
            let tmpPlayCounter = 0;

    //Redux sound instance
             tmp.map(async function(instance){
                try{
                  await instance.stopAsync();
                }
                catch (e) {
                    console.log("Error to stop instance of sound")
                    console.log(e)
                }
            });

            tmp.push(sound);
            this.props.addVideo(tmp);
            this.playbackInstance = sound;
    //Redux audio title
            tmpplayer.push(this.state.selectedItem.title);
            this.props.addPlayerBar(tmpplayer);
    //Redux audio stop/play to player bar
            tmpplayerBarPlay = false;
            console.log("this.state.isPlaying", this.state.isPlaying);

            this.props.addPlayerBarPlay(tmpplayerBarPlay);

            this.props.addPlayCounter(tmpPlayCounter);

        }
        this._updateScreenForLoading(false);
    }

    _mountVideo = component => {

        this._video = component;
        this._loadNewPlaybackInstance(true);
    };

    _updateScreenForLoading(isLoading) {
        if (isLoading) {
            this.setState({
                showVideo: false,
                isPlaying: false,
                playbackInstanceName: LOADING_STRING,
                playbackInstanceDuration: null,
                playbackInstancePosition: null,
                isLoading: true
            });
        } else {
            this.setState({
                playbackInstanceName: this.state.PLAYLIST[this.index].name,
                showVideo: this.state.PLAYLIST[this.index].isVideo,
                isLoading: false
            });
        }
    }

    _onPlaybackStatusUpdate = status => {
        if (status.isLoaded) {
            this.setState({
                playbackInstancePosition: status.positionMillis,
                playbackInstanceDuration: status.durationMillis,
                shouldPlay: status.shouldPlay,
                isPlaying: status.isPlaying,
                isBuffering: status.isBuffering,
                rate: status.rate,
                muted: status.isMuted,
                volume: status.volume,
                loopingType: status.isLooping ? LOOPING_TYPE_ONE : LOOPING_TYPE_ALL,
                shouldCorrectPitch: status.shouldCorrectPitch
            });
            if (status.didJustFinish && !status.isLooping) {
                this._advanceIndex(true);
                this._updatePlaybackInstanceForIndex(true);
            }
        } else {
            if (status.error) {
                console.log(`FATAL PLAYER ERROR: ${status.error}`);
            }
        }
    };

    _onLoadStart = () => {
        console.log(`ON LOAD START`);
    };

    _onLoad = status => {
        console.log(`ON LOAD : ${JSON.stringify(status)}`);
    };

    _onError = error => {
        console.log(`ON ERROR : ${error}`);
    };

    _onReadyForDisplay = event => {
        const widestHeight =
            (DEVICE_WIDTH * event.naturalSize.height) / event.naturalSize.width;
        if (widestHeight > VIDEO_CONTAINER_HEIGHT) {
            this.setState({
                videoWidth:
                    (VIDEO_CONTAINER_HEIGHT * event.naturalSize.width) /
                    event.naturalSize.height,
                videoHeight: VIDEO_CONTAINER_HEIGHT
            });
        } else {
            this.setState({
                videoWidth: DEVICE_WIDTH,
                videoHeight:
                    (DEVICE_WIDTH * event.naturalSize.height) / event.naturalSize.width
            });
        }
    };

    _onFullscreenUpdate = event => {
        console.log(
            `FULLSCREEN UPDATE : ${JSON.stringify(event.fullscreenUpdate)}`
        );
    };

    _advanceIndex(forward) {
        this.index =
            (this.index + (forward ? 1 : this.state.PLAYLIST.length - 1)) % this.state.PLAYLIST.length;
    }

    async _updatePlaybackInstanceForIndex(playing) {
        this._updateScreenForLoading(true);
        this.setState({
            videoWidth: DEVICE_WIDTH,
            videoHeight: VIDEO_CONTAINER_HEIGHT
        });

        this._loadNewPlaybackInstance(playing);
    }

    _onPlayPausePressed = () => {
        if (this.playbackInstance != null) {

            if (this.state.isPlaying) {
                this.playbackInstance.pauseAsync();
                this.props.addPlayerBarPlay(false);
                this.props.addPlayCounter(1);
            } else {
                this.playbackInstance.playAsync();
                this.props.addPlayerBarPlay(true);
                // this.props.addPlayCounter(1);
            }
        }
    };

    _onStopPressed = () => {
        if (this.playbackInstance != null) {
            this.playbackInstance.stopAsync();
        }
    };

    _onMutePressed = () => {
        if (this.playbackInstance != null) {
            this.playbackInstance.setIsMutedAsync(!this.state.muted);
        }
    };

    _onVolumeSliderValueChange = value => {
        if (this.playbackInstance != null) {
            this.playbackInstance.setVolumeAsync(value);
        }
    };

    _trySetRate = async (rate, shouldCorrectPitch) => {
        if (this.playbackInstance != null) {
            try {
                await this.playbackInstance.setRateAsync(rate, shouldCorrectPitch);
            } catch (error) {
                // Rate changing could not be performed, possibly because the client's Android API is too old.
            }
        }
    };

    _onSeekSliderValueChange = value => {
        if (this.playbackInstance != null && !this.isSeeking) {
            this.isSeeking = true;
            this.shouldPlayAtEndOfSeek = this.state.shouldPlay;
            this.playbackInstance.pauseAsync();
        }
    };

    _onSeekSliderSlidingComplete = async value => {
        if (this.playbackInstance != null) {
            this.isSeeking = false;
            const seekPosition = value * this.state.playbackInstanceDuration;
            if (this.shouldPlayAtEndOfSeek) {
                this.playbackInstance.playFromPositionAsync(seekPosition);
            } else {
                this.playbackInstance.setPositionAsync(seekPosition);
            }
        }
    };

    _getSeekSliderPosition() {
        if (
            this.playbackInstance != null &&
            this.state.playbackInstancePosition != null &&
            this.state.playbackInstanceDuration != null
        ) {
            return (
                this.state.playbackInstancePosition /
                this.state.playbackInstanceDuration
            );
        }
        return 0;
    }

    _getMMSSFromMillis(millis) {
        const totalSeconds = millis / 1000;
        const seconds = Math.floor(totalSeconds % 60);
        const minutes = Math.floor(totalSeconds / 60);

        const padWithZero = number => {
            const string = number.toString();
            if (number < 10) {
                return "0" + string;
            }
            return string;
        };
        return padWithZero(minutes) + ":" + padWithZero(seconds);
    }

    _getTimestamp() {
        if (
            this.playbackInstance != null &&
            this.state.playbackInstancePosition != null &&
            this.state.playbackInstanceDuration != null
        ) {
            return `${this._getMMSSFromMillis(
                this.state.playbackInstancePosition
            )} / ${this._getMMSSFromMillis(this.state.playbackInstanceDuration)}`;
        }
        return "";
    }

    _onPosterPressed = () => {
        this.setState({ poster: !this.state.poster });
    };

    _onUseNativeControlsPressed = () => {
        this.setState({ useNativeControls: !this.state.useNativeControls });
    };

    _onFullscreenPressed = () => {
        try {
            this._video.presentFullscreenPlayer();
        } catch (error) {
            console.log(error.toString());
        }
    };

    _onPressButton = () => {
        console.log('hidden?',this.state.hidden);
        this.setState({
            hidden:!this.state.hidden
        });
    };

    _setPlayerBarPlay = () => {
        if (this.playbackInstance != null) {
console.log('preseed');
                this.props.addPlayCounter(1);
                this.props.addPlayerBarPlay(true);
        }
    };

    render() {

        return !this.state.fontLoaded ? (
            <View style={styles.emptyContainer} />
        ) : (

            <TouchableOpacity onPress={this._onPressButton}
                              style={styles.container}
            >
                {/*{!this.state.hidden ? (*/}
                <ImageBackground
                source={{uri : this.state.selectedItem.image}}
                imageStyle={{opacity:0.9}}
                style={styles.container}
            >
                <View />
                {/*<TouchableOpacity onPress={this._onPressButton}*/}
                {/*                  style={styles.container}*/}
                {/*>*/}
                {!this.state.hidden ? (
                    <TouchableOpacity
                        underlayColor={BACKGROUND_COLOR}
                        style={styles.wrapper}
                        onPress={() =>
                            // this.props.navigation.navigate('Meditation')}
                        {this.props.navigation.dispatch(SwitchActions.jumpTo(
                                { routeName: `${this.state.PLAYLIST[this.index].categoryName}Stack`}));

                            this._setPlayerBarPlay();
                        }}

                        disabled={this.state.isLoading}
                    >
                        <MaterialIcons
                            name={ICON_EXPAND_MORE}
                            size={60}
                            color="black"
                        />
                    </TouchableOpacity>
                ) : null}
                {!this.state.hidden ? (
                    <View style={styles.nameContainer}>
                        <Text style={[styles.text, { fontFamily: "roboto-regular" }]}>
                            {this.state.playbackInstanceName}
                        </Text>
                    </View>
                ) : null}
                {/*{!this.state.hidden ? (*/}
                    <View style={styles.videoContainer}>
                        {!this.state.hidden ? (

                            <Card
                            containerStyle={styles.card}
                            title={"Author: " + this.state.selectedItem.title}
                            titleStyle={styles.author}
                        >
                            <Text style={[styles.text1, { fontFamily: "roboto-regular" }]}>
                                {this.state.selectedItem.description}
                            </Text>

                        </Card>
                        ) : null}

                        <Video
                            ref={this._mountVideo}
                            style={[
                                styles.video,
                                {
                                    opacity: this.state.showVideo ? 1.0 : 0.0,
                                    width: this.state.videoWidth,
                                    height: this.state.videoHeight
                                }
                            ]}
                            resizeMode={Video.RESIZE_MODE_CONTAIN}
                            onPlaybackStatusUpdate={this._onPlaybackStatusUpdate}
                            onLoadStart={this._onLoadStart}
                            onLoad={this._onLoad}
                            onError={this._onError}
                            onFullscreenUpdate={this._onFullscreenUpdate}
                            onReadyForDisplay={this._onReadyForDisplay}
                            useNativeControls={this.state.useNativeControls}
                        />
                    </View>
                 {/*) : null}*/}
                {!this.state.hidden ? (
                    <View
                        style={[
                            styles.playbackContainer,
                            {
                                opacity: this.state.isLoading ? DISABLED_OPACITY : 1.0
                            }
                        ]}
                    >
                        <Slider
                            style={styles.playbackSlider}
                            minimumTrackTintColor = {Colors.greenLight}
                            maximumTrackTintColor = {Colors.greyLight}
                            thumbTintColor = {Colors.greenLight}
                            trackImage={ICON_TRACK_1}
                            // thumbImage={ICON_THUMB_1}
                            value={this._getSeekSliderPosition()}
                            onValueChange={this._onSeekSliderValueChange}
                            onSlidingComplete={this._onSeekSliderSlidingComplete}
                            disabled={this.state.isLoading}
                        />
                        <View style={styles.timestampRow}>
                            <Text
                                style={[
                                    styles.textBuffering,
                                    styles.buffering,
                                    { fontFamily: "roboto-regular" }
                                ]}
                            >
                                {this.state.isBuffering ? BUFFERING_STRING : ""}
                            </Text>
                            <Text
                                style={[
                                    styles.text,
                                    styles.timestamp,
                                    { fontFamily: "roboto-regular" }
                                ]}
                            >
                                {this._getTimestamp()}
                            </Text>
                        </View>
                    </View>
                ) : null}
                {!this.state.hidden ? (
                    <View
                        style={[
                            styles.buttonsContainerBase,
                            styles.buttonsContainerTopRow,
                            {
                                opacity: this.state.isLoading ? DISABLED_OPACITY : 1.0
                            }
                        ]}
                    >
                        <TouchableOpacity
                            underlayColor={BACKGROUND_COLOR}
                            style={styles.wrapper}
                            onPress={this._onPlayPausePressed}
                            disabled={this.state.isLoading}
                        >
                            <MaterialIcons
                                name={
                                    this.state.isPlaying
                                        ? ICON_PAUSE_BUTTON
                                        : ICON_PLAY_BUTTON
                                }
                                size={60}
                                color= {Colors.greenLight}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            underlayColor={BACKGROUND_COLOR}
                            style={styles.wrapper}
                            onPress={this._onStopPressed}
                            disabled={this.state.isLoading}
                        >
                            <MaterialIcons
                                name={ICON_STOP_BUTTON}
                                size={60}
                                color= {Colors.greenLight}
                            />
                        </TouchableOpacity>
                    </View>
                ) : null}

                {/*<View*/}
                {/*    style={[*/}
                {/*        styles.buttonsContainerBase,*/}
                {/*        styles.buttonsContainerMiddleRow*/}
                {/*    ]}*/}
                {/*>*/}
                {/*    <View style={styles.volumeContainer}>*/}
                {/*        <TouchableOpacity*/}
                {/*            underlayColor={BACKGROUND_COLOR}*/}
                {/*            style={styles.wrapper}*/}
                {/*            onPress={this._onMutePressed}*/}
                {/*        >*/}
                {/*            <MaterialIcons*/}
                {/*                name={*/}
                {/*                    this.state.muted*/}
                {/*                        ? ICON_MUTED_BUTTON*/}
                {/*                        : ICON_UNMUTED_BUTTON*/}
                {/*                }*/}
                {/*                size={32}*/}
                {/*                color="black"*/}
                {/*            />*/}
                {/*        </TouchableOpacity>*/}
                {/*        <Slider*/}
                {/*            style={styles.volumeSlider}*/}
                {/*            minimumTrackTintColor = {Colors.grey}*/}
                {/*            maximumTrackTintColor = {Colors.greyLight}*/}
                {/*            trackImage={ICON_TRACK_1}*/}
                {/*            thumbImage={ICON_THUMB_2}*/}
                {/*            value={1}*/}
                {/*            onValueChange={this._onVolumeSliderValueChange}*/}
                {/*        />*/}
                {/*    </View>*/}
                {/*</View>*/}
                <View />
                {this.state.showVideo ? (
                    <View>
                        <View
                            style={[
                                styles.buttonsContainerBase,
                                styles.buttonsContainerTextRow
                            ]}
                        >
                            <View />
                            <TouchableOpacity
                                underlayColor={BACKGROUND_COLOR}
                                style={styles.wrapper}
                                onPress={this._onPosterPressed}
                            >
                                <View style={styles.button}>
                                    <Text
                                        style={[styles.text, { fontFamily: "roboto-regular" }]}
                                    >
                                        Poster: {this.state.poster ? "yes" : "no"}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <View />
                            <TouchableOpacity
                                underlayColor={BACKGROUND_COLOR}
                                style={styles.wrapper}
                                onPress={this._onFullscreenPressed}
                            >
                                <View style={styles.button}>
                                    <Text
                                        style={[styles.text, { fontFamily: "roboto-regular" }]}
                                    >
                                        Fullscreen
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <View />
                        </View>
                        <View style={styles.space} />
                        <View
                            style={[
                                styles.buttonsContainerBase,
                                styles.buttonsContainerTextRow
                            ]}
                        >
                            <View />
                            <TouchableOpacity
                                underlayColor={BACKGROUND_COLOR}
                                style={styles.wrapper}
                                onPress={this._onUseNativeControlsPressed}
                            >
                                <View style={styles.button}>
                                    <Text
                                        style={[styles.text, { fontFamily: "roboto-regular" }]}
                                    >
                                        Native Controls:{" "}
                                        {this.state.useNativeControls ? "yes" : "no"}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <View />
                        </View>
                    </View>
                ) : null}

                {/*</TouchableOpacity>*/}
            {/*</View>*/}
            </ImageBackground>
                 {/*) :*/}
                 {/*    <ImageBackground*/}
                 {/*        source={{uri : this.state.selectedItem.image}}*/}
                 {/*        style={styles.container}*/}
                 {/*    >*/}
                 {/*    </ImageBackground>*/}
                 {/*}*/}
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    emptyContainer: {
        alignSelf: "stretch",
        backgroundColor: BACKGROUND_COLOR,
    },
    container: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "center",
        alignSelf: "stretch",
        backgroundColor: BACKGROUND_COLOR,
        // opacity: 0.3

    },
    wrapper: {},
    descContainer: {
        height: FONT_SIZE,
        top: 10
    },
    nameContainer: {
        height: FONT_SIZE,
        top: -5
    },
    space: {
        height: FONT_SIZE
    },
    card: {
        borderWidth: 0, // Remove Border
        shadowColor: 'rgba(0,0,0, 0.0)', // Remove Shadow IOS
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0,
        shadowRadius: 0,
        elevation: 0, // This is for Android
        backgroundColor: '#00000000'
    },
    videoContainer: {
        top: -20,
        maxHeight: VIDEO_CONTAINER_HEIGHT,
        height: DEVICE_HEIGHT/2.8
    },
    video: {
        maxWidth: DEVICE_WIDTH,
        maxHeight: DEVICE_HEIGHT/4
    },
    playbackContainer: {
        top: 10,
        flex: 1,
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "center",
        alignSelf: "stretch",
        maxHeight: 38,
        minHeight: 38,
    },
    playbackSlider: {
        alignSelf: "stretch",

    },
    timestampRow: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        alignSelf: "stretch",
        minHeight: FONT_SIZE
    },
    text: {
        fontSize: FONT_SIZE + 8,
        minHeight: FONT_SIZE + 8,
        // fontWeight: "bold",
        backgroundColor:'rgba(0,0,0,.3)',
        borderRadius:5,
        textAlign: 'center',
        color: "white",
        overflow: 'hidden'
    },
    text1: {
        fontSize: FONT_SIZE + 4,
        minHeight: FONT_SIZE + 4,
        // fontWeight: "bold",
        backgroundColor:'rgba(0,0,0,.3)',
        borderRadius:5,
        textAlign: 'center',
        color: "white",
        overflow: 'hidden',
        top: 30
    },
    textBuffering: {
        fontSize: FONT_SIZE + 8,
        minHeight: FONT_SIZE + 8,
        // fontWeight: "bold",
        color: "white",
        overflow: 'hidden'
    },
    author: {
        fontSize: FONT_SIZE + 4,
        minHeight: FONT_SIZE + 4,
        // fontWeight: "bold",
        backgroundColor:'rgba(0,0,0,.3)',
        borderRadius:5,
        textAlign: 'center',
        color: "white",
        overflow: 'hidden',
        paddingTop: 5,
        paddingBottom: 5
    },
    buffering: {
        textAlign: "left",
        paddingLeft: 20
    },
    timestamp: {
        textAlign: "right",
        // paddingRight: 20
        right: 20
    },
    button: {
        backgroundColor: BACKGROUND_COLOR
    },
    buttonsContainerBase: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    buttonsContainerTopRow: {
        maxHeight: 51,
        minWidth: DEVICE_WIDTH / 2.0,
        maxWidth: DEVICE_WIDTH / 2.0
    },
    buttonsContainerMiddleRow: {
        maxHeight: 58,
        alignSelf: "stretch",
        paddingRight: 20
    },
    volumeContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        minWidth: DEVICE_WIDTH / 1.2,
        maxWidth: DEVICE_WIDTH / 1.2,
        paddingLeft: 20
    },
    volumeSlider: {
        width: DEVICE_WIDTH / 1.2
    },
    buttonsContainerBottomRow: {
        maxHeight: 19,
        alignSelf: "stretch",
        paddingRight: 20,
        paddingLeft: 20
    },
    rateSlider: {
        width: DEVICE_WIDTH / 2.0
    },
    buttonsContainerTextRow: {
        maxHeight: FONT_SIZE,
        alignItems: "center",
        paddingRight: 20,
        paddingLeft: 20,
        minWidth: DEVICE_WIDTH,
        maxWidth: DEVICE_WIDTH
    }
});

const mapDispatchToProps = dispatch => {
    return{
        addVideo: (video) => {
            dispatch(setVideo(video))
        },
        addPlayerBar: (player) => {
            dispatch(setPlayerBar(player))
        },
        addPlayerBarPlay: (playerBarPlay) => {
            dispatch(setPlayerBarPlay(playerBarPlay))
        },
        addPlayCounter: (playCounter) => {
            dispatch(setPlayCounter(playCounter))
        }
    }
}

const mapStateToProps = state => {
    return {
        selectedItem: state.selectedItem,
        setVideo: state.video,
        setPlayerBar: state.player,
        setPlayerBarPlay: state.playerBarPlay,
        setPlayCounter: state.playCounter
    }
}

export default withNavigation (connect(mapStateToProps, mapDispatchToProps)(Player))
