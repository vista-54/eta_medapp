import React from "react";
import {
  StyleSheet,
  View,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Text,
  Dimensions
} from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getArticles, getCategories, getSubcategories} from '../actions/collection'
import {activateKeepAwake} from "expo-keep-awake";
import { YellowBox } from 'react-native';

const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get("window");
YellowBox.ignoreWarnings(['Setting a timer']);
console.ignoredYellowBox = ['Setting a timer'];
console.ignoredYellowBox = ['enter'];

class SplashScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      subcategories: [],
      categories: [],
      articles: [],
      medSubCategories: [],
      loading:  true,
    };
    this.load = this.load.bind(this);
    this.load();
    console.ignoredYellowBox = ['Setting a timer'];
    console.ignoredYellowBox = ['enter'];

  };

  componentDidMount() {
    this.counter = 1;
  }

  async load(){
    await this.props.getArticles();
    await this.props.getCategories();
    await this.props.getSubcategories();
    this.setState({
      loading: false
    })
  }
  _activate = () => {
    activateKeepAwake();
  };
  static navigationOptions = {
    title: 'SplashScreen',
  };

  render() {
    this.counter++;
    console.log('RENDERCOUNTER===Splash', this.counter);
    const {navigate} = this.props.navigation;

    if(this.props) {
      const {articles, categories, subcategories, medSubCategories} = this.props;
    }

    return (
      <View>
        {this.state.loading ?
          (
            <View style={styles.container}>
              <StatusBar hidden={true} />
              <ImageBackground
                  source={require("../assets/images/forest.jpg")}
                  resizeMode="contain"
                  style={styles.image}
                  imageStyle={styles.image_imageStyle}

              >
              </ImageBackground>
            </View>

          )
          :
          (
            <View style={styles.container}>
              <StatusBar hidden={true} />
              <ImageBackground
                  source={require("../assets/images/forest.jpg")}
                  resizeMode="contain"
                  style={styles.image}
                  imageStyle={styles.image_imageStyle}

              >
                <TouchableOpacity onPress={ () => navigate('Meditation')}>
                  <View style={styles.groupStack}>
                    <Text style={styles.textStart}>
                          {/*Touch to Start*/}
                    </Text>
                    {/*<View style={styles.group}>*/}
                      {/*<View style={styles.rect}>*/}

                      {/*  /!*<Svg viewBox="0 0 15.49 15.49" style={styles.ellipse}>*!/*/}
                      {/*  /!*  <Ellipse*!/*/}
                      {/*  /!*      strokeWidth={1}*!/*/}
                      {/*  /!*      fill="rgba(255,255,255,1)"*!/*/}
                      {/*  /!*      cx={8}*!/*/}
                      {/*  /!*      cy={8}*!/*/}
                      {/*  /!*      rx={7}*!/*/}
                      {/*  /!*      ry={7}*!/*/}
                      {/*  /!*  />*!/*/}
                      {/*  /!*</Svg>*!/*/}
                      {/*</View>*/}
                    {/*</View>*/}
                    <Icon name="chevron-down" style={styles.icon}/>
                  </View>
                </TouchableOpacity>
              </ImageBackground>
            </View>
          )
        }
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    width: 1757,
    height: 925,
    marginTop: -57,
    marginLeft: -742,
  },
  group: {
    top: 26,
    width: 22,
    height: 61,
    position: "absolute",
    opacity: 0.77,
    left: 5
  },
  rect: {
    width: 22,
    height: 61,
    borderRadius: 30,
    borderColor: "rgba(255,255,255,1)",
    borderWidth: 2
  },
  ellipse: {
    width: 15,
    height: 15,
    marginTop: 36,
    marginLeft: 3
  },
  textStart: {
  color: "white"
  },
  icon: {
    top: 0,
    position: "absolute",
    color: "rgba(255,255,255,1)",
    fontSize: 31,
    transform: [
      {
        rotate: "180.00deg"
      }
    ],
    opacity: 0.77,
    height: 31,
    width: 31,
    left: 0
  },
  groupStack: {
    width: 32,
    height: 87,
    marginTop: DEVICE_HEIGHT,
    marginLeft: 726 + (DEVICE_WIDTH/2)
  }
});
const mapDispatchToProps = dispatch => {
  console.log('mapdispatchtoprops');
  return bindActionCreators({
    getArticles,
    getCategories,
    getSubcategories
  }, dispatch)
}

const mapStateToProps = state => {
  return {
    articles: state.articles.articles,
    categories: state.categories.categories,
    subcategories: state.subcategories.subcategories,
  }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SplashScreen)
